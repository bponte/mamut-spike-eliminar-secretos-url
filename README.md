

## Usando [Spark's "Hadoop Free" Build](http://spark.apache.org/docs/latest/hadoop-provided.html)

Se uso una spark 2.2.0 sin las librerías de hadoop y hadoop 2.9(a la fecha, la última versión)

Esta versión de hadoop ya solventa el problema que deseamos solucionar y es la opción recomendada


## Usando Spark spark-2.2.0-bin-hadoop2.7

Se uso la estrategia de incluir las dependencias de hadoop 2.9 en el uber jar

La ejecución de la aplicación standalone se termina según lo esperado

Usando spark-submit se origina error de dependencias, y el uber jar generado tiende a ser > 200 Mb para una aplicación
simple

## Run

> spark-submit  --class cl.tidchile.mamut.spike.s3.amazon --master local[4] S3Spike-assembly-1.0.jar \
    bucket_name_1 accessKey_1 secretKey_1 path_1 bucket_name_2 accessKey_2 secretKey_2 path_2


