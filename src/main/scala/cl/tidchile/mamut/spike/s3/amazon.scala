package cl.tidchile.mamut.spike.s3

import org.apache.spark.sql._

import org.slf4j.{Logger, LoggerFactory}

object amazon {
  val logger: Logger = LoggerFactory.getLogger(getClass)
  import logger._

  def main(args: Array[String]): Unit = {

    info("STARTED")

    if (args.length < 8) {

      error("invalid number of arguments Usage: bucket_name_1 accessKey_1 secretKey_1 path_1 bucket_name_2 accessKey_2 secretKey_2 path_2")

      System.exit(1)

    }

    val ACCESS_KEY_CONF = "fs.s3a.bucket.%s.access.key"

    val SECRET_KEY_CONF = "fs.s3a.bucket.%s.secret.key"

    val bucketName1 = args(0)//"mamut.dev.tidchile.cl"

    val accessKey1 = args(1) //"AKIAJUIZDLUPC5UH4BIQ"

    val secretKey1 = args(2) // "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"

    val path1 = args(3) // "3lSdp0vZMOS8gy5tycn67wPvRnI4krsWWNNfeizd"

    val bucketName2 = args(4)//"mamut.pr.tidchile.cl"

    val accessKey2 = args(5) //"AKIAJUIZDLUPC5UH4BIQ"

    val secretKey2 = args(6) // "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"

    val path2 = args(7)

    val accessKeyConf1 = ACCESS_KEY_CONF.format(bucketName1)

    val secretKeyConf1 = SECRET_KEY_CONF.format(bucketName1)

    val accessKeyConf2 = ACCESS_KEY_CONF.format(bucketName2)

    val secretKeyConf2 = SECRET_KEY_CONF.format(bucketName2)

    var hadoopConfiguration = Map[String, String]()

    hadoopConfiguration +=("fs.s3a.impl"-> "org.apache.hadoop.fs.s3a.S3AFileSystem")

    hadoopConfiguration += (accessKeyConf1 -> accessKey1)

    hadoopConfiguration += (secretKeyConf1 -> secretKey1)

    hadoopConfiguration += (accessKeyConf2 -> accessKey2)

    hadoopConfiguration += (secretKeyConf2 -> secretKey2)



    val spark: SparkSession = SparkSession
      .builder
      .master("local[2]")
      .appName("spike amazon s3 hadoop 2.9")
      .getOrCreate()


    info("SparkContext master is: %s".format(spark.sparkContext.master))

    info("applicationId is: %s".format(spark.sparkContext.applicationId))


    if (hadoopConfiguration != null) {

      hadoopConfiguration.foreach(entry => spark.sparkContext.hadoopConfiguration.set(entry._1, entry._2))

    }

    spark.sparkContext.getConf.getAll.foreach(entry => info("%s: %s".format(entry._1, entry._2)))

    val URL_TEMPLATE = "s3a://%s/%s"

    val URL1 = URL_TEMPLATE.format(bucketName1, path1)

    val URL2 = URL_TEMPLATE.format(bucketName2, path2)

    info("URL 1: %s".format(URL1))

    info("URL 2: %s".format(URL2))

    val df: DataFrame = spark.read.format("csv").load(URL1)

    df.show()

    val df2: DataFrame = spark.read.format("csv").load(URL2)

    df2.show()

    spark.stop()
  }

}
